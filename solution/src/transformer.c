#include "img_errors.h"
#include "img.h"
#include "transformer.h"
#include <stdlib.h>

// 90 deg clockwise
struct image rotate(struct image const source) {
    uint32_t height = source.height;
    uint32_t width = source.width;

    struct image rotated_img = create_image(height, width); 

    for (size_t x = 0; x < width; x++) {
        for (size_t y = 0; y < height; y++) {
            size_t new_x = y;
            size_t new_y = width - x - 1;
            rotated_img.data[new_y * height + new_x] = source.data[y * width + x];
        }
    }
    return rotated_img;
}

ImageResult rotate_by_angle(struct image const source, int32_t angle, struct image *rotated_img) {
    if (angle % 90 != 0) {
        return IMAGE_ERR_INVALID_ANGLE;
    }
    
    angle = (angle % 360 + 360) % 360;
    size_t rotations = angle / 90;

    *rotated_img = create_image(source.width, source.height);
    if (rotated_img->data == NULL) {
        return IMAGE_ERR_ALLOCATION;
    }

    copy_image_data(&source, rotated_img);

    struct pixel* old_data = NULL;

    for (size_t i = 0; i < rotations; i++) {
        struct image temp_img = rotate(*rotated_img);

        old_data = rotated_img->data;

        *rotated_img = temp_img;

        if (old_data != rotated_img->data) {
            free(old_data);
        }

        if (rotated_img->data == NULL) {
            return IMAGE_ERR_ALLOCATION;
        }

        if (i == 0) {
            rotated_img->width = source.height;
            rotated_img->height = source.width;
        }
    }

    return IMAGE_OK;
}
