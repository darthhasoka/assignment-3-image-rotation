#include "img.h"
#include <stdlib.h>

struct image create_image(uint32_t width, uint32_t height) {
    struct pixel* pixels = malloc(sizeof(struct pixel) * width * height);
    if (!pixels) {
        return (struct image){
            .width = 0,
            .height = 0,
            .data = NULL
        }; 
    }
    return (struct image){
        .width = width,
        .height = height,
        .data = pixels
    };
}

void copy_image_data(struct image const* source, struct image* destination) {
    for (size_t i = 0; i < source->height * source->width; i++) {
        destination->data[i] = source->data[i];
    }
}

void delete_image(struct image* img) {
    if (img && img->data) {
        free(img->data);
        img->data = NULL;
    }
}
