#include "utils.h"
#include "bmp.h"


struct bmp create_bmp_header(struct image const* img) {
    return (struct bmp) {
        .bfType = BMP_TYPE,
        .bfileSize = sizeof(struct bmp) + CALCULATE_IMG_SIZE(img),
        .bfReserved = DEFAULT,
        .bOffBits = sizeof(struct bmp),
        .biSize = BMP_HEAD_SIZE,
        .biWidth = img -> width,
        .biHeight = img -> height,
        .biPlanes = BMP_PLANES,
        .biBitCount = BIT_COLOR,
        .biCompression = DEFAULT,
        .biSizeImage = CALCULATE_IMG_SIZE(img),
        .biXPelsPerMeter = DEFAULT,
        .biYPelsPerMeter = DEFAULT,
        .biClrUsed = DEFAULT,
        .biClrImportant = DEFAULT
    };
}
