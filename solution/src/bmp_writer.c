#include "bmp.h"
#include "utils.h"
#include <stdbool.h>
#include <stdlib.h>

#define BMP_MAX_PADDING_SIZE 3


static bool write_pixel_row(FILE* out, const struct pixel* pixels, size_t width) {
    return fwrite(pixels, sizeof(struct pixel), width, out) == width;
}

static bool write_padding(FILE* out, uint32_t padding) {
    uint8_t pad[BMP_MAX_PADDING_SIZE] = {0}; 
    return fwrite(pad, 1, padding, out) == padding;
}

static bool write_pixels(FILE* out, struct image const* img) {
    for (size_t row = 0; row < img->height; row++) {
        const struct pixel* row_pixels = img->data + row * img->width;
        if (!write_pixel_row(out, row_pixels, img->width)) {
            return false;
        }

        uint32_t padding = CALCULATE_PADDING(img->width);
        if (!write_padding(out, padding)) {
            return false;
        }
    }
    return true;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp head = create_bmp_header(img);

    // writing header of bmp
    if (fwrite(&head, sizeof(struct bmp), 1, out) != 1) {
        return WRITE_ERROR;
    }
    // writing pixels of image
    if (!write_pixels(out, img)) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}
