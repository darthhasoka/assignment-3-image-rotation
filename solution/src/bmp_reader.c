#include "bmp.h"
#include "utils.h"
#include <stdbool.h>
#include <stdlib.h>

#define READ_PIXEL_ROW(in, pixels, width) (fread((pixels), sizeof(struct pixel), (width), (in)) == (width))
#define SKIP_PADDING(in, padding) (fseek((in), (padding), SEEK_CUR))


static bool read_pixels(FILE* in, struct image* img) {
    struct pixel* pixels = malloc(sizeof(struct pixel) * img->width * img->height);
    if (pixels == NULL) {
        return false;
    }

    for (size_t row = 0; row < img->height; row++) {
        if (!READ_PIXEL_ROW(in, pixels + row * img->width, img->width)) {
            free(pixels);
            return false;
        }
        uint32_t padding = CALCULATE_PADDING(img->width);
        SKIP_PADDING(in, padding);
    }

    img->data = pixels;
    return true;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp head;
    //reading header of bmp
    if(fread(&head, sizeof(struct bmp), 1, in) != 1) {
        return READ_ERROR;
    }

    img->height = head.biHeight;
    img->width = head.biWidth;

    //reding pixels of inage
    if (!read_pixels(in, img)) {
        return READ_ERROR;
    }

    return READ_OK;
}
