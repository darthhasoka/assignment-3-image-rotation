
#include "img_errors.h"
#include "bmp.h"
#include "transformer.h"
#include <stdio.h>
#include <stdlib.h>

#define EXPECTED_ARGC 4

int main( int argc, char **argv ) {

    if(argc != EXPECTED_ARGC) {
        return 1;
    }

    int32_t angle;
    if (sscanf(argv[3], "%d", &angle) != 1) {
        fprintf(stderr, "Err: Invalid angle: %s is not a valid integer", argv[3]);
        return 2;
    }
    
    FILE* in = fopen(argv[1], "rb");
    if (in == NULL) {
        return 2;
    }

    FILE* out = fopen(argv[2], "wb");
    if (out == NULL) {
        fclose(in);
        return 2;
    }

    // reading image from file
    struct image img;
    if (from_bmp(in, &img) != READ_OK) {
        perror("Error reading source image file");
        fclose(in);
        fclose(out);
        return 4;
    }
    fclose(in); // closing file afterwards


    struct image rotated_img;
    ImageResult result = rotate_by_angle(img, angle, &rotated_img);
    if (result == IMAGE_ERR_INVALID_ANGLE) {
        fprintf(stderr, "Err: Invalid angle provided");
        return 1;
    } else if (result == IMAGE_ERR_ALLOCATION) {
        fprintf(stderr, "Err: Memory allocation failed");
        return 1;
    }
    delete_image(&img); 

    // writing image to file
    if (to_bmp(out, &rotated_img) != WRITE_OK) {
        fprintf(stderr, "Write err: Can't write to bmp");
        delete_image(&rotated_img);
        fclose(out);
        return 5;
    }

    // freeing sources
    fclose(out);
    delete_image(&rotated_img);
    return 0;
}

