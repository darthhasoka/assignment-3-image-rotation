#ifndef UTILS_H
#define UTILS_H

#include "bmp.h"

uint32_t calculate_padding(uint32_t width);
uint32_t calculate_img_size(struct image const* img);
struct bmp create_bmp_header(struct image const* img);

#endif

#define CALCULATE_PADDING(width) (((BMP_PADDING - ((width) * sizeof(struct pixel)) % BMP_PADDING)) % BMP_PADDING)
#define CALCULATE_IMG_SIZE(img) ((((img)->width * sizeof(struct pixel)) + CALCULATE_PADDING((img)->width)) * (img)->height)

