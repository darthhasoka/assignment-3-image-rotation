#include <stdint.h>

#ifndef IMAGE_ERRORS_H
#define IMAGE_ERRORS_H

typedef enum {
    IMAGE_OK,
    IMAGE_ERR_ALLOCATION,
    IMAGE_ERR_INVALID_ANGLE
} ImageResult;

#endif

