#ifndef BMP_H
#define BMP_H

#define BMP_PADDING 4
#define BMP_TYPE 0x4D42
#define BIT_COLOR 24
#define BMP_HEAD_SIZE 40
#define BMP_PLANES 1
#define DEFAULT 0

#include <stdint.h>
#include <stdio.h>

#include "img.h"
#include "text_messages.h"

#pragma pack(push, 1)
struct bmp {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);

#endif 

