#include "img_errors.h"

struct image rotate(struct image const source);
ImageResult rotate_by_angle(struct image const source, int32_t angle, struct image *rotated_img);

