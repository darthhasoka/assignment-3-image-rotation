
#include <stdint.h>

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image create_image(uint32_t width, uint32_t height);
void copy_image_data(struct image const* source, struct image* destination);
void delete_image(struct image* img);

